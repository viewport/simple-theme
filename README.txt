The Simple Theme for Scroll Viewport is an example Theme.

1. In order to get use the Bootstrap Theme you have two options

* If you have the Atlassian SDK installed, do a atlas-cli and pi to build and
upload the theme as a plugin.

* Otherwise, start your Confluence server with Scroll Viewport and FTP enabled,
create a new top-level folder "bootstrap-theme" and upload everything under
src/main/resources (except atlassian-plugin.xml).

2. To Change the color scheme of the theme change the css link in the
   "html-header.vm" file, for the colors you can choose between

    Two skins : dark, white
    Five accent color : blue, green, orange, red, turquoise



