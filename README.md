Scroll Viewport
===============
Simple Theme
---------------
![Simple Theme Thumbnail](https://bytebucket.org/viewport/simple-theme/raw/2d2f02fee37a559c8430e3a883973a79b4093bce/img/thumbnail.png)

The Simple Theme for Scroll Viewport is based on Thememarts Symple Theme.

In order to install this theme 

1. With viewport already installed in your system enable FTP through the admin console. 
1. With any FTP client connect to the system and create a new top-level folder - "bootstrap-theme" for example.
1. Upload every single file from this folder.
1. Assign a viewport to the space you want under VIEWPORTS in the admin console.
1. Go to the space and under Tools, select Open Viewport. 

To Change the color scheme of the theme change the css link in the
   "html-header.vm" file, for the colors you can choose between

**Two skins :** dark, white (default)

**Five accent color :** blue, green (default), orange, red, turquoise 

For more information please visit the [Scroll Viewport documentation](http://www.k15t.com/display/VPRT/Documentation). 

Features
--------

* Slideshow support
* Home / Children Pages 
* Basic Blog support


Need Help?
----------

If you have any questions please visit 

* [Viewport Documentation Pages](http://www.k15t.com/display/VPRT/Documentation)
* [Scroll Viewport Developers Group](https://groups.google.com/forum/#!forum/scroll-viewport-dev).

Copyright  
---------

K15t Software GmbH [@k15t](http://twitter.com/k15tsoftware)
Licensed under the Apache License, Version 2.0 

Third party
---------------------------------------------------

ThemeSmarts                 Created by [ThemeSmarts](http://themesmrts.com)

jQuery                      Copyright (c) jQuery Foundation.

Cycle2              MIT     Created by [M. Alsup](http://jquery.malsup.com/cycle2/)

prettify                    Code at [Google Code](https://code.google.com/p/google-code-prettify/)